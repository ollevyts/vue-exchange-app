import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/ExchangeMain')
  },
  {
    path: '/result',
    name: 'result',
    props: { data: true },
    component: () => import('../views/ExchangeResult')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
